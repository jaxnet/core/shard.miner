module gitlab.com/jaxnet/core/shard.miner.git

go 1.14

require (
	github.com/btcsuite/btclog v0.0.0-20170628155309-84c8d2346e9f
	github.com/btcsuite/go-socks v0.0.0-20170105172521-4720035b7bfd
	github.com/btcsuite/websocket v0.0.0-20150119174127-31079b680792
	github.com/btcsuite/winsvc v1.0.0
	github.com/jessevdk/go-flags v1.4.0
	github.com/jrick/logrotate v1.0.0
	github.com/minio/sha256-simd v0.1.1
	gitlab.com/jaxnet/core/shard.core.git v0.0.0-20200729135904-1f77d5308d1c
	golang.org/x/crypto v0.0.0-20200728195943-123391ffb6de
)
